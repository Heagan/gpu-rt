from timeit import default_timer as timer   
from math import sqrt, inf
from _thread import start_new_thread

from draw import *

cdef int HEIGHT = 500
cdef int WIDTH =  500
cdef int MAX_DEPTH = 3
cdef int SPLITS = 30

cdef double RATIO = float(HEIGHT) / WIDTH

class Vector:
    def __init__(self, double x, double y, double z):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        if isinstance(other, (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(other, (int, float)):
            other = Vector(other, other, other)
        return Vector(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z
            )
    
    def __sub__(self, other):
        if isinstance(other, (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(other, (int, float)):
            other = Vector(other, other, other)
        return Vector(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z
            )
    
    def __mul__(self, other):
        if isinstance(other, (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(other, (int, float)):
            other = Vector(other, other, other)
        return Vector(
            self.x * other.x,
            self.y * other.y,
            self.z * other.z
            )

    def __truediv__(self, other):
        if isinstance(other, (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(other, (int, float)):
            other = Vector(other, other, other)
        return Vector(
            self.x / other.x,
            self.y / other.y,
            self.z / other.z
            )

    def __pow__(self, other):
        if isinstance(other, (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(other, (int, float)):
            other = Vector(other, other, other)
        return Vector(
            self.x ** other.x,
            self.y ** other.y,
            self.z ** other.z
            )
    
    def __itruediv__(self, other):
        if isinstance(type(other), (tuple)):
            other = Vector(other[0], other[1], other[2])
        if isinstance(type(other), (int, float)):
            other = Vector(other, other, other)
            
        self.x /= other
        self.y /= other
        self.z /= other

    def __str__(self):
        return f"{self.x}, {self.y}, {self.z}"

    def length2(self):
        return (self.x ** 2) + (self.y ** 2) + (self.z ** 2)

    def length(self):
        return sqrt(self.length2())
    
    def norm(self):
        l = self.length()
        self /= l

    @staticmethod    
    def normalize(v):
        l = v.length()
        return v / l

    @staticmethod
    def dot(a, b):
        return a.x * b.x + a.y * b.y + a.z * b.z

    @staticmethod
    def cross(a, b):
        return Vector(
            a.y * b.z - a.z * b.x ,
            a.z * b.x - a.x * b.z,
            a.x * b.y -a.y * b.x
            )

    @staticmethod    
    def vsqrt(v):
        return Vector(
            sqrt(v.x),
            sqrt(v.y),
            sqrt(v.z)
        )



def reflected(vector, axis):
    return vector - axis * Vector.dot(vector, axis) * 2


def nearest_intersected_object(objects, ray_origin, ray_direction):
    def sphere_intersect(center, radius, ray_origin, ray_direction):
        b = 2 * Vector.dot(ray_direction, ray_origin - center)
        c = (ray_origin - center).length2() - radius ** 2

        delta = b ** 2 - c * 4
        if delta > 0:
            t1 = (-b + sqrt(delta)) / 2
            t2 = (-b - sqrt(delta)) / 2
            if t1 > 0 and t2 > 0:
                return min(t1, t2)
        return None

    distances = [sphere_intersect(obj['center'], obj['radius'], ray_origin, ray_direction) for obj in objects]
    nearest_object = None
    min_distance = inf
    for index, distance in enumerate(distances):
        if distance and distance < min_distance:
            min_distance = distance
            nearest_object = objects[index]

    return nearest_object, min_distance


def trace(int x, int y):
    spheres = [
        {'center': Vector(-0.2, 0, -1), 'radius': 0.7, 'ambient': Vector(0.1, 0, 0),
            'diffuse': Vector(0.7, 0, 0), 'specular': Vector(1, 1, 1), 'shininess': 100, 'reflection': 0.5},
        {'center': Vector(0.1, -0.3, 0), 'radius': 0.1, 'ambient': Vector(0.1, 0, 0.1),
            'diffuse': Vector(0.7, 0, 0.7), 'specular': Vector(1, 1, 1), 'shininess': 100, 'reflection': 0.5},
        {'center': Vector(0, -9000, 0), 'radius': 9000 - 0.7, 'ambient': Vector(0.1, 0.1, 0.1),
            'diffuse': Vector(0.6, 0.6, 0.6), 'specular': Vector(1, 1, 1), 'shininess': 100, 'reflection': 0.5}
    ]
    
    light = {
        'position': Vector(5, 5, 5),
        'ambient':  Vector(1, 1, 1),
        'diffuse':  Vector(1, 1, 1),
        'specular': Vector(1, 1, 1)
    }

    cdef int reflection = 2

    cdef double j = float(2 * x) / WIDTH - 1
    cdef double i = float(-2 * y) / HEIGHT + 1
    screen = (-1, 1 / RATIO, 1, -1 / RATIO)
    
    pixel = Vector(j, i, 0)
    cam = Vector(0, 0, 1)
    origin = cam
    direction = Vector.normalize(pixel - origin)
    color = Vector(0, 0, 0)

    for k in range(MAX_DEPTH):
        nearest_object, min_distance = nearest_intersected_object(spheres, origin, direction)
        if nearest_object is None:
            break

        intersection = direction * min_distance + origin
        normal_to_surface = Vector.normalize(intersection - nearest_object['center'])
        shifted_point = normal_to_surface * 1e-5 + intersection
        intersection_to_light = Vector.normalize(light['position'] - shifted_point)
        _, min_distance = nearest_intersected_object(spheres, shifted_point, intersection_to_light)

        intersection_to_light_distance = (light['position'] - intersection).length2()
        
        is_shadowed = min_distance < intersection_to_light_distance

        if is_shadowed:
            break

        illumination = Vector(0, 0, 0)
        illumination += nearest_object['ambient'] * light['ambient']
        illumination += nearest_object['diffuse'] * light['diffuse'] * Vector.dot(intersection_to_light,
                                                                                normal_to_surface)
        intersection_to_camera = Vector.normalize(cam - intersection)
        _h = Vector.normalize(intersection_to_light + intersection_to_camera)
        illumination += nearest_object['specular'] * light['specular'] * \
                        Vector.dot(normal_to_surface, _h) ** (nearest_object['shininess'] / 4)
        color += illumination * reflection
        reflection *= nearest_object['reflection']
        origin = shifted_point
        direction = reflected(direction, normal_to_surface)
    c = (round(color.x * 255), round(color.y  * 255), round(color.z * 255) )
    putpixel(x, y, c)
    start_new_thread(update, ())


def split_trace(int x1, int x2, int y1, int y2):
        for yy in range(y1, y2):
            for xx in range(x1, x2):
                trace(xx, yy)

def execute():
    # global GO
   
    split_trace(0, WIDTH, 0, HEIGHT)
    
#    for sy in range(SPLITS):
#        for sx in range(SPLITS):
#            sx1 = WIDTH // SPLITS * (sx - 1)
#            sy1 = HEIGHT // SPLITS * (sy - 1)
#            sx2 = WIDTH // SPLITS * sx
#            sy2 = HEIGHT // SPLITS * sy
#            split_trace(sx1, sx2, sy1, sy2)
            #start_new_thread(split_trace, (sx1, sx2, sy1, sy2))
    
def main():
    
    init("RT", (WIDTH, HEIGHT))
    update()
    start = timer() 
    execute()
    print("with GPU:", timer()-start)
    while True:
        update()

if __name__ == '__main__':
    main()

